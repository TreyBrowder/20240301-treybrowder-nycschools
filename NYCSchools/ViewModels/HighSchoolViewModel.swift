//
//  HighSchoolViewModel.swift
//  NYCSchools
//
//  Created by Trey Browder on 2/28/24.
//

import Foundation

///View model responsible for managing high school data.
class HighSchoolViewModel {
    
    ///Array containing high school information.
    private var highSchoolArr: [HighSchoolInfo] = []
    
    ///computed property to return the number of schools in the array of high schools
    var numberOfSchools: Int {
        return highSchoolArr.count
    }
    
    /// Retrieves the high school information at the specified index.
    ///Parameters
    /// - `index`:  The index of the high school information to retrieve.
    /// - returns:   HighSchool object from the specified index
    func getSchoolDetail(at index: Int) -> HighSchoolInfo {
        return highSchoolArr[index]
    }
    
    ///Sort the Schools by name in the High school data arry
    func sortSchoolsByName() {
        highSchoolArr.sort {
            $0.school_name < $1.school_name
        }
    }
    
    ///Add a method to get the filtered list of schools based on text entered in the search box
    func filteredSchools(for searchText: String) -> [HighSchoolInfo] {
        return highSchoolArr.filter {
            $0.school_name.lowercased().contains(searchText.lowercased())
        }
    }
    
    /// Fetches high school data from an external API.
    /// - Parameter completion: A closure to be executed when the data fetching is completed.
    func fetchData(completion: @escaping () -> Void) {
        guard let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") else {
            print("Invalid URL for High School Data")
            return
        }
        print("URL for 2017 High School Data is valid")
        
        //dont need the response
        URLSession.shared.dataTask(with: url) { [weak self] (data, _, error) in
            guard let self = self else {
                return
            }
            
            if let error = error {
                print("Error fetching 2017 High School data: \(error.localizedDescription)")
                return
            }
            
            guard let data = data else {
                print("No High School data received")
                return
            }
            
            //test to make sure I was getting the data back from URL session
            //print("here is the High School data from the URL:")
            //print(String(data: data, encoding: .utf8) ?? "Data not in UTF-8 format")
            
            do {
                self.highSchoolArr = try JSONDecoder().decode([HighSchoolInfo].self, from: data)
                DispatchQueue.main.async {
                    
                    completion()
                }
            } catch {
                print("Error decoding data: \(error.localizedDescription)")
            }
        }.resume()
    }
}

