//
//  SATDataViewModel.swift
//  NYCSchools
//
//  Created by Trey Browder on 2/28/24.
//
//
//

import Foundation

///View model to populate SAT Data if its available
class SATDataViewModel {
    
    ///Fetch data related to SAT Data for each high school if it exist
    func fetchData(for dbn: String, completion: @escaping (SATData?) -> Void) {
        //print school dbn debugging issue
        print("dbn for selected school: \(dbn)")
        guard let url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(dbn)") else {
            print("Invalid URL for pulling SAT Data")
            completion(nil)
            return
        }
        print("SAT Data URL is valid...Proceeding...")
        
        //dont need response
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            if let error = error {
                print("---------- URL Session Error ----------")
                print("Error fetching SAT data: \(error.localizedDescription)")
                completion(nil)
                return
            }
            
            guard let data = data else {
                print("No SAT data received")
                completion(nil)
                return
            }
            print("----Data successfully unwrapped for the URL session----")
            
            //was getting errors due to data having invalid format or doesnt exist
            print("here is the SAT data from the URL: \(data)")
            print(String(data: data, encoding: .utf8) ?? "Data not in UTF-8 format")
            
            do {
                let satData = try JSONDecoder().decode([SATData].self, from: data).first
                completion(satData)
            } catch {
                print("Error decoding SAT data: \(error.localizedDescription)")
                completion(nil)
            }
        }.resume()
    }
}

