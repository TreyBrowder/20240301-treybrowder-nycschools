//
//  extensions.swift
//  NYCSchools
//
//  Created by Trey Browder on 2/28/24.
//


import Foundation
import UIKit

//Helper computed properties should i decide to manually set frames in view did layout method
extension UIView {
    public var width: CGFloat {
        return frame.size.width
    }
    
    public var height: CGFloat {
        return frame.size.height
    }
    
    public var top: CGFloat {
        return frame.origin.y
    }
    
    public var bottom: CGFloat {
        return frame.size.height + frame.origin.y
    }
    
    public var left: CGFloat {
        return frame.origin.x
    }
    
    public var right: CGFloat {
        return frame.size.width + frame.origin.x
    }
}
