//
//  ViewController.swift
//  NYCSchools
//
//  Created by Trey Browder on 2/28/24.
//

import UIKit

///View controller to display a list of high schools.
class HighSchoolViewController: UIViewController {
    
// MARK: - View properties
    
    //UI Table connected via storyboard
    @IBOutlet var tableView: UITableView!
    
    //ViewModel obj for High School list
    public lazy var highSchoolViewModel: HighSchoolViewModel = {
        return HighSchoolViewModel()
    }()
    
    //Array for search bar results
    public var schoolsSearchedArr: [HighSchoolInfo] = []
    
    //view title
    private lazy var titleLabel: UILabel = {
        var label = UILabel()
        label.text = "New York High Schools"
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    //search bar view
    public lazy var searchBarView: UISearchBar = {
        let searchView = UISearchBar()
        searchView.placeholder = "Search by School Name"
        
        return searchView
    }()
    
    
    
    
// MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(titleLabel)
        view.addSubview(searchBarView)
        
        //set up Delegates/register the cell identifier
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(HighSchoolTableViewCell.self, forCellReuseIdentifier: "HighSchoolCell")
        searchBarView.delegate = self
        
        loadTableData()
        
        // Add tap gesture recognizer to dismiss keyboard
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(dismissKeyboard))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
        //also add to the table view to dissmiss the keyboard if user interacts with the table view
        tableView.addGestureRecognizer(tapGesture)

    }
    
    //manually set positioning
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // Position the title label
        titleLabel.frame = CGRect(x: 20,
                                  y: view.safeAreaInsets.top + 20,
                                  width: view.bounds.width - 40,
                                  height: 50)
        
        // Position the search bar
        searchBarView.frame = CGRect(x: 20,
                                     y: titleLabel.frame.maxY + 20,
                                     width: view.bounds.width - 40,
                                     height: 50)
        
        // Position the table view
        tableView.frame = CGRect(x: 0,
                                 y: searchBarView.frame.maxY + 20,
                                 width: view.bounds.width,
                                 height: view.bounds.height - searchBarView.frame.maxY - 20)
    }
    
// MARK: - View Data Methods
    
    private func loadTableData() {
        highSchoolViewModel.fetchData { [weak self] in
            self?.highSchoolViewModel.sortSchoolsByName()
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
    }
    
// MARK: - @objc Methods
    
    @objc private func dismissKeyboard() {
        searchBarView.resignFirstResponder()
    }
}

// MARK: - Table view extension delegate & data source

extension HighSchoolViewController: UITableViewDataSource, UITableViewDelegate{
    
    // MARK: conform to data source protocol
    
    //func populates the table with all the returned cells based on the data coming in from the API
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = HighSchoolTableViewCell(style: .default, reuseIdentifier: "HighSchoolCell")
        var school = highSchoolViewModel.getSchoolDetail(at: indexPath.row)
        
        //if the search bar "is active" populate table based on the search
        if isFiltering() {
            school = schoolsSearchedArr[indexPath.row]
        } else {
            school = highSchoolViewModel.getSchoolDetail(at: indexPath.row)
        }
        
        //need to add word wrap property since names of
        //schools is running passed view bounds
        cell.nameLabel.text = school.school_name
        cell.nameLabel.numberOfLines = 0
        cell.nameLabel.lineBreakMode = .byWordWrapping
        
        cell.phoneNumberLabel.text = school.phone_number
        cell.emailLabel.text = school.school_email ?? "Email: N/A"
        cell.websiteLabel.text = school.website ?? "Website: N/A"
        
        if let address = school.primary_address_line_1,
           let city = school.city,
           let state = school.state_code,
           let zip = school.zip {
            cell.addressLabel.text = "\(address), \(city), \(state) \(zip)"
        } else {
            cell.addressLabel.text = "Address: N/A"
        }

        
        return cell
    }
    
    // MARK: return cell hieight
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }
    
    //conform to delegate protocol
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isFiltering() {
            return schoolsSearchedArr.count
        }
        
        return highSchoolViewModel.numberOfSchools
    }
    
    // MARK: User selected Row
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //unhighlight the selected cell
        tableView.deselectRow(at: indexPath, animated: true)
        
        //sets the school var to the highschool obj based on index of the cell user tapped
        var school = highSchoolViewModel.getSchoolDetail(at: indexPath.row)
        
        //if user is searching for a school
        if isFiltering() {
            school = schoolsSearchedArr[indexPath.row]
        } else {
            //if user isn't searching then populate table from viewModel
            school = highSchoolViewModel.getSchoolDetail(at: indexPath.row)
        }
        
        // Create a dispatch group before initiating SAT data fetch because
        // its an async task and you want it to finish before trying to present
        //the data or displaying error messages
        let group = DispatchGroup()
        
        // Enter the dispatch group before starting the SAT data fetch task
        group.enter()
        
        //fetch SAT Data asynchronously
        lazy var satDataViewModel = SATDataViewModel()
        satDataViewModel.fetchData(for: school.dbn) { [weak self] satDataResult in
            DispatchQueue.main.async {
                guard let self = self else {
                    return
                }
                
                //once SAT data task is done defer block is called
                //to ensure .leave() is called for code clean up purposes
                defer {
                    // Leave the dispatch group
                    group.leave()
                }
                
                //Tried to unwrap the school name to make more clear error logs to debug
                //this is when i discovered not every school in the 2017 NYC high api is in
                //the 2012 SAT score api
                //guard let schoolName = satData?.school_name else {
                //  print("Error: couldn't unwrap school name")
                //  return
                //}
                
                //unwrap the satData result from the fetch
                if let satData = satDataResult {
                    
                    //edge case: if number of test takers is 's'
                    if satData.num_of_sat_test_takers == "s" {
                        // Display an alert
                        let alert = UIAlertController(title: "Invalid Data",
                                                      message: "\(school.school_name) didn't report valid SAT scores.",
                                                      preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default))
                        self.present(alert, animated: true)
                    } else {
                        // Create and present SATDataViewController
                        let satDataVC = SATDataViewController(satData: satData)
                        let navController = UINavigationController(rootViewController: satDataVC)
                        self.present(navController, animated: true)
                    }
                } else {
                    //edge case: Display an alert if no SAT data is available
                    print("This isn't an ERROR: No data available for: \(school.school_name)")
                    let alert = UIAlertController(title: "Whoops!",
                                                  message: "\(school.school_name) doesn't have any public SAT scores yet!",
                                                  preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: .default))
                    self.present(alert, animated: true)
                }
            }
        }
        
        //Notify when tasks in dispatch group are done
        group.notify(queue: .main) {
            print("SAT data async task is done")
        }
    }
    
    //True - if text
    //false - if no text
    ///Helper method to determine if the search bar is active and text is being entered
    private func isFiltering() -> Bool {
        return !(searchBarView.text ?? "").isEmpty
    }
}

// MARK: SearchBar Delegate

extension HighSchoolViewController: UISearchBarDelegate {
    
    //update and filter the table as user is typing in the searchbar with textDidChange function
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        schoolsSearchedArr = highSchoolViewModel.filteredSchools(for: searchText)
        tableView.reloadData()
    }
    
    //end editing on the keyboard so dismiss the keyboard
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        // Dismiss the keyboard
        searchBar.resignFirstResponder()
        return true
    }
    
    //user clicked Search button - dismiss keyboard
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

// MARK: - UIScrollViewDelegate - to dismiss the keyboard when user starts to scroll

extension HighSchoolViewController: UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBarView.resignFirstResponder()
    }
}

