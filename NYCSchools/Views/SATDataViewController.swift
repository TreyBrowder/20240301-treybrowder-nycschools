//
//  SATDataViewController.swift
//  NYCSchools
//
//  Created by Trey Browder on 2/28/24.
//

import UIKit

///View Controller to show SAT average test data
class SATDataViewController: UIViewController {

//MARK: Properties
    
    /// The SAT data associated with the school.
    var satData: SATData
    
    private let newYorkImgView: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "newyorkVC")
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    //create UI Labels to hold each piece of info to display
    //Name of school
    //number of test takes
    //avg scores for reading, math, writing
    
    private lazy var schoolNameLabel: UILabel = {
        let label = UILabel()
        label.text = "\(satData.school_name)"
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: UIFont.labelFontSize)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping

        return label
    }()
    
    private lazy var numTestTakersLabel: UILabel = {
        let label = UILabel()
        label.text = "Number of students that took the SAT: \(satData.num_of_sat_test_takers)"
        label.textAlignment = .center
        
        return label
    }()
    
    private lazy var readAvgLabel: UILabel = {
        let label = UILabel()
        label.text = "Average Reading score: \(satData.sat_critical_reading_avg_score)"
        label.textAlignment = .center
        
        return label
    }()
    
    private lazy var mathAvgLabel: UILabel = {
        let label = UILabel()
        label.text = "Average Math score: \(satData.sat_math_avg_score)"
        label.textAlignment = .center
        
        return label
    }()
    
    private lazy var writeAvgLabel: UILabel = {
        let label = UILabel()
        label.text = "Average writing score: \(satData.sat_writing_avg_score)"
        label.textAlignment = .center
        
        return label
    }()
    
    init(satData: SATData) {
        self.satData = satData
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
//MARK: View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("-------- LOG -------- SATDataViewController")
        print("LOG -------- View Did load method")
        
        view.backgroundColor = .systemBackground
        
        // Display school SAT data here
        view.addSubview(newYorkImgView)
        view.addSubview(schoolNameLabel)
        view.addSubview(numTestTakersLabel)
        view.addSubview(readAvgLabel)
        view.addSubview(mathAvgLabel)
        view.addSubview(writeAvgLabel)
        
        setupConstraints()
        
    }
    
    //Tried to use viewDidLayout func at first to set up the UIView
    //but setting up the hard coded values was too time consuming and inefficient in this use case
    
    //function to dynamically set up properties
    private func setupConstraints() {
        
        //Allow for dynamic layout based on the view bounds of device
        newYorkImgView.translatesAutoresizingMaskIntoConstraints = false
        schoolNameLabel.translatesAutoresizingMaskIntoConstraints = false
        numTestTakersLabel.translatesAutoresizingMaskIntoConstraints = false
        readAvgLabel.translatesAutoresizingMaskIntoConstraints = false
        mathAvgLabel.translatesAutoresizingMaskIntoConstraints = false
        writeAvgLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            //Constraints for newYorkImgView
            newYorkImgView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            newYorkImgView.widthAnchor.constraint(equalToConstant: view.bounds.width / 3),
            newYorkImgView.heightAnchor.constraint(equalToConstant: view.bounds.width / 3),
            newYorkImgView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            //Constraints for schoolNameLabel
            schoolNameLabel.topAnchor.constraint(equalTo: newYorkImgView.bottomAnchor, constant: 20),
            schoolNameLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
            schoolNameLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30),
            
            //Constraints for numTestTakersLabel
            numTestTakersLabel.topAnchor.constraint(equalTo: schoolNameLabel.bottomAnchor, constant: 20),
            numTestTakersLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
            numTestTakersLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30),
            
            //Constraints for readAvgLabel
            readAvgLabel.topAnchor.constraint(equalTo: numTestTakersLabel.bottomAnchor, constant: 20),
            readAvgLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
            readAvgLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30),
            
            //Constraints for mathAvgLabel
            mathAvgLabel.topAnchor.constraint(equalTo: readAvgLabel.bottomAnchor, constant: 20),
            mathAvgLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
            mathAvgLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30),
            
            //Constraints for writeAvgLabel
            writeAvgLabel.topAnchor.constraint(equalTo: mathAvgLabel.bottomAnchor, constant: 20),
            writeAvgLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30),
            writeAvgLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30),
            writeAvgLabel.bottomAnchor.constraint(lessThanOrEqualTo: view.bottomAnchor, constant: -30)
        ])
    }
}
