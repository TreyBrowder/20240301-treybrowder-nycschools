//
//  HighSchoolTableViewCell.swift
//  NYCSchools
//
//  Created by Trey Browder on 2/28/24.
//

import UIKit

///Custom table view cell for displaying high school information.
class HighSchoolTableViewCell: UITableViewCell {
    let nameLabel = UILabel()
    let phoneNumberLabel = UILabel()
    let emailLabel = UILabel()
    let websiteLabel = UILabel()
    let addressLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupLabels()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    ///Configures the labels and their constraints within the cell.
    private func setupLabels() {
        
        //Customize labels
        nameLabel.font = UIFont.boldSystemFont(ofSize: 16)
        phoneNumberLabel.font = UIFont.systemFont(ofSize: 14)
        emailLabel.font = UIFont.systemFont(ofSize: 14)
        websiteLabel.font = UIFont.systemFont(ofSize: 14)
        addressLabel.font = UIFont.systemFont(ofSize: 14)
        
        //Add labels to the cell's contentView
        contentView.addSubview(nameLabel)
        contentView.addSubview(phoneNumberLabel)
        contentView.addSubview(emailLabel)
        contentView.addSubview(websiteLabel)
        contentView.addSubview(addressLabel)
        
        //Configure label constraints
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        phoneNumberLabel.translatesAutoresizingMaskIntoConstraints = false
        emailLabel.translatesAutoresizingMaskIntoConstraints = false
        websiteLabel.translatesAutoresizingMaskIntoConstraints = false
        addressLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            nameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            nameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8),
            
            phoneNumberLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 8),
            phoneNumberLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            
            emailLabel.topAnchor.constraint(equalTo: phoneNumberLabel.bottomAnchor, constant: 8),
            emailLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            
            websiteLabel.topAnchor.constraint(equalTo: emailLabel.bottomAnchor, constant: 8),
            websiteLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            
            addressLabel.topAnchor.constraint(equalTo: websiteLabel.bottomAnchor, constant: 8),
            addressLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            addressLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8)
        ])
    }
}

