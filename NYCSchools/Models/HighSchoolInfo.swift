//
//  HighSchoolInfo.swift
//  NYCSchools
//
//  Created by Trey Browder on 2/28/24.
//

import Foundation

struct HighSchoolInfo: Codable {
    let dbn: String
    let school_name: String
    let phone_number: String
    let school_email: String?
    let website: String?
    let primary_address_line_1: String?
    let city: String?
    let state_code: String?
    let zip: String?
}
