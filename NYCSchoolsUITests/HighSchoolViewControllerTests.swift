//
//  HighSchoolViewControllerTests.swift
//  NYCSchoolsUITests
//
//  Created by Trey Browder on 3/3/24.
//

import XCTest
@testable import NYCSchools

class HighSchoolViewControllerTests: XCTestCase {
    
    var viewController: HighSchoolViewController!
    
    override func setUp() {
        super.setUp()
        
        // Instantiate the view controller from the storyboard
        let myBundle = Bundle(for: type(of: self))
        let storyboard = UIStoryboard(name: "Main", bundle: myBundle)
        viewController = storyboard.instantiateViewController(withIdentifier: "HighSchoolViewController") as? HighSchoolViewController
        viewController.loadViewIfNeeded()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    ///Test if the table View is not nil
    func testTableViewExist() {
        XCTAssertNotNil(viewController.tableView)
    }
    
    ///Tests the search bar filtering functionality.
    func testSearchBarFiltering() {
        //Set search text for filtering action
        viewController.searchBarView.text = "Test"
        viewController.searchBar(viewController.searchBarView, textDidChange: "Test")
        
        //Check if the number of rows in the table view matches the number of filtered schools
        XCTAssertEqual(viewController.tableView.numberOfRows(inSection: 0), viewController.schoolsSearchedArr.count)
    }
    
    ///Tests the dismissal of the keyboard when scrolling begins.
    func testDismissKeyboard() {
        //scroll view Scroll action
        viewController.tableView.contentOffset = CGPoint(x: 0, y: 100)
        viewController.scrollViewWillBeginDragging(viewController.tableView)
        
        //Check if the search bar is no longer the first responder
        XCTAssertFalse(viewController.searchBarView.isFirstResponder)
    }
    
    ///Tests the behavior of the searchBarShouldEndEditing method.
    func testSearchBarShouldEndEditing() {
        //End editing of the search bar
        let endEditing = viewController.searchBarShouldEndEditing(viewController.searchBarView)
        
        //Check if the method returns true and the search bar is not the first responder
        XCTAssertTrue(endEditing)
        XCTAssertFalse(viewController.searchBarView.isFirstResponder)
    }
    
    ///Tests the behavior of the searchBarSearchButtonClicked method.
    func testSearchBarSearchButtonClicked() {
        //Mock tapping the search button
        viewController.searchBarSearchButtonClicked(viewController.searchBarView)
        
        //Check if the search bar is no longer the first responder
        XCTAssertFalse(viewController.searchBarView.isFirstResponder)
    }
}
