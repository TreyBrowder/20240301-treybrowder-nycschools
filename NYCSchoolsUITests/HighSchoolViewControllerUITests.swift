//
//  HighSchoolViewControllerUITests.swift
//  NYCSchoolsUITests
//
//  Created by Trey Browder on 2/28/24.
//

import XCTest
@testable import NYCSchools

class HighSchoolViewControllerUITests: XCTestCase {
    
    var viewController: HighSchoolViewController!
    
    override func setUp() {
        super.setUp()
        
        // Instantiate the view controller from the storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        viewController = storyboard.instantiateViewController(withIdentifier: "HighSchoolViewController") as? HighSchoolViewController
        viewController.loadViewIfNeeded()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    /// Tests if the table view data source methods are correctly implemented.
    func testTableViewDataSource() {
        // Check if the number of rows in the table view matches the number of schools in the view model
        XCTAssertEqual(viewController.tableView.numberOfRows(inSection: 0), viewController.highSchoolViewModel.numberOfSchools)
        
        // Check if the table view cell is successfully dequeued
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = viewController.tableView(viewController.tableView, cellForRowAt: indexPath)
        XCTAssertNotNil(cell)
    }
    
    /// Tests the search bar filtering functionality.
    func testSearchBarFiltering() {
        // Set search text to trigger filtering
        viewController.searchBarView.text = "Test"
        viewController.searchBar(viewController.searchBarView, textDidChange: "Test")
        
        // Check if the number of rows in the table view matches the number of filtered schools
        XCTAssertEqual(viewController.tableView.numberOfRows(inSection: 0), viewController.schoolsSearchedArr.count)
    }
    
    /// Tests the selection of a row in the table view.
    func testTableViewRowSelection() {
        // Simulate selection of a row
        let indexPath = IndexPath(row: 0, section: 0)
        viewController.tableView.delegate?.tableView?(viewController.tableView, didSelectRowAt: indexPath)
        
        // Check if the presented view controller is an instance of SATDataViewController
        XCTAssertTrue(viewController.presentedViewController is SATDataViewController)
    }
    
    /// Tests the dismissal of the keyboard when scrolling begins.
    func testDismissKeyboard() {
        // Simulate scroll view dragging
        viewController.tableView.contentOffset = CGPoint(x: 0, y: 100)
        viewController.scrollViewWillBeginDragging(viewController.tableView)
        
        // Check if the search bar is no longer the first responder
        XCTAssertFalse(viewController.searchBarView.isFirstResponder)
    }
    
    /// Tests the behavior of the searchBarShouldEndEditing method.
    func testSearchBarShouldEndEditing() {
        // Simulate end editing of the search bar
        let shouldEndEditing = viewController.searchBarShouldEndEditing(viewController.searchBarView)
        
        // Check if the method returns true and the search bar is not the first responder
        XCTAssertTrue(shouldEndEditing)
        XCTAssertFalse(viewController.searchBarView.isFirstResponder)
    }
    
    /// Tests the behavior of the searchBarSearchButtonClicked method.
    func testSearchBarSearchButtonClicked() {
        // Simulate tapping the search button on the search bar
        viewController.searchBarSearchButtonClicked(viewController.searchBarView)
        
        // Check if the search bar is no longer the first responder
        XCTAssertFalse(viewController.searchBarView.isFirstResponder)
    }
}
