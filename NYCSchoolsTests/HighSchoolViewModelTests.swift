//
//  HighSchoolViewModelTests.swift
//  NYCSchoolsUITests
//
//  Created by Trey Browder on 2/29/24.
//

import XCTest
@testable import NYCSchools

class HighSchoolViewModelTests: XCTestCase {
    
// MARK: - Properties
    
    var viewModel: HighSchoolViewModel!
    
// MARK: - Setup
    
    override func setUp() {
        super.setUp()
        viewModel = HighSchoolViewModel()
    }
    
    override func tearDown() {
        viewModel = nil
        super.tearDown()
    }
    
// MARK: - Test Cases
    
    ///Tests the initialization of the view model.
    func testInitialization() {
        XCTAssertNotNil(viewModel, "ViewModel should not be nil after initialization")
        XCTAssertTrue(viewModel.numberOfSchools == 0, "Number of schools should initially be 0")
    }
    
    ///Tests the fetching of high school data from an API.
    func testFetchData() {
        let expectation = self.expectation(description: "Fetch data expectation")
        
        viewModel.fetchData {
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertTrue(viewModel.numberOfSchools > 0, "Number of schools should be greater than 0 after fetching data")
    }
}

